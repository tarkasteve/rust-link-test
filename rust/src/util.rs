
use rand::random;

#[no_mangle]
pub extern "C" fn rand_init() {
}

#[no_mangle]
pub extern "C" fn rand16() -> u16 {
    random()
}

#[no_mangle]
pub extern "C" fn rand32() -> u32 {
    random()
}

#[no_mangle]
pub extern "C" fn rand64() -> u64 {
    random()
}
