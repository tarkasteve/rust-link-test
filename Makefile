
TARGET   = main
CC       = gcc
CFLAGS   = -std=c99 -Wall
LINKER   = gcc
UNAME    = $(shell uname -s)
LDFLAGS   = -Wall -L $(RUSTLIB_DIR) -l $(RUSTLIB)
ifeq ($(UNAME),Linux)
	LDFLAGS += -lpthread -lm -ldl
endif
ifeq ($(UNAME),Darwin)
	LDFLAGS += -framework Security
endif

SOURCES  := $(wildcard *.c)
OBJECTS  := $(SOURCES:%.c=%.o)

RUSTDIR  = rust
RUSTLIB = util_rust
RUSTLIB_DIR = $(RUSTDIR)/target/release
RUSTLIB_FILE = $(RUSTLIB_DIR)/lib$(RUSTLIB).a



$(TARGET): $(OBJECTS) $(RUSTLIB_FILE)
	$(LINKER) $(OBJECTS) $(LDFLAGS) -o $@

$(OBJECTS): %.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(RUSTLIB_FILE):
	make -C $(RUSTDIR)

clean:
	rm -f $(OBJECTS) $(TARGET)
	make -C $(RUSTDIR) clean

.PHONY: clean
