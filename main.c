
#include <stdio.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

void rand_init();
u16 rand16(void);
u32 rand32(void);
u64 rand64(void);


int main (int argc, char **argv)
{
  rand_init();
  unsigned short id = rand16();
  printf("Rand is %d\n", id);
}
